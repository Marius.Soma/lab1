package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true) {
        System.out.println("Let's play round " + roundCounter);
        roundCounter++;
        System.out.println("Your choice (Rock/Paper/Scissors)?");
        String svar = sc.nextLine();
        String svarLower = svar.toLowerCase();

        if(!svarLower.equals("rock") && !svarLower.equals("paper") && !svarLower.equals("scissors")) {
			System.out.println("I do not understand "+ svar +". Could you try again?");
        }else{
			int rand = (int)(Math.random()*3);
            String aiSvar = "";
			if(rand == 0) {
				aiSvar = "rock";
			} else if(rand == 1) {
				aiSvar = "paper";
			} else {
				aiSvar = "scissors";
			}
            String winner = "";
            if(aiSvar.equals(svar.toLowerCase())){
                System.out.println("Human chose "+svar.toLowerCase()+", computer chose "+aiSvar+". It's a tie!");
            }else if (svar.equals("Rock") && aiSvar.equals("paper")|| svar.equals("Scissors") && aiSvar.equals("rock") || svar.equals("Paper") && aiSvar.equals("scissors")){
                winner = "Computer";
                computerScore++;
                System.out.println("Human chose "+svar.toLowerCase()+", computer chose "+aiSvar+". "+winner+" wins!");
            }else {
                winner = "Human";
                humanScore++;
                System.out.println("Human chose "+svar.toLowerCase()+", computer chose "+aiSvar+". "+winner+" wins!");
            }
            System.out.println("Score: human "+ humanScore+", computer "+computerScore);
            
        }
    System.out.println("Do you wish to continue playing? (y/n)?");
    String fortsette = sc.nextLine();
    if(fortsette.equals("n")){
        System.out.println("Bye bye :)");
        break;
    }
}
}

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
